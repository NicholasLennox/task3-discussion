package no.noroff.nicholas.heroes;

/*
    Hero properties
    -----------------------
    -Stats
    -Level
    -Equipment (list of items)
    -Can be a class

    Hero behaviours
    --------------------
    -Level up
    -Equip items
    -Attack
    */

import java.util.ArrayList;

public class Hero {
    // Properties
    // Stats
    private int health;
    private int strength;
    private int dexterity;
    private int intelligence;
    // Level
    private int currentLevel = 1;
    private int xpToNextLevel = 100; // 100@1, 110@2, 121@3, ...
    // Items
    ArrayList<String> equipment = new ArrayList<>();
    // Class
    private String heroClass;

    // Behaviours
    public void levelUp(){
        currentLevel++;
        xpToNextLevel = (int)(xpToNextLevel*1.1);
        System.out.println("Hero levelled up!");
        System.out.println("They are now level " + currentLevel);
        System.out.println("They need " + xpToNextLevel + " xp to level up");
    }



    public void equipItem(String item){
        equipment.add(item);
        System.out.println("The hero has the following equipment:");
        for (String it: equipment) {
            System.out.println(it);
        }
    }

    public void attack(){
        System.out.println("Attacks with all their might");
    }

    // Constructor

    public Hero() {
    }

    public Hero(int health, int strength, int dexterity, int intelligence, int currentLevel,
                int xpToNextLevel, ArrayList<String> equipment, String heroClass) {
        this.health = health;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.currentLevel = currentLevel;
        this.xpToNextLevel = xpToNextLevel;
        this.equipment = equipment;
        this.heroClass = heroClass;
    }
    // Getters and setters
    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public int getXpToNextLevel() {
        return xpToNextLevel;
    }

    public void setXpToNextLevel(int xpToNextLevel) {
        this.xpToNextLevel = xpToNextLevel;
    }

    public ArrayList<String> getEquipment() {
        return equipment;
    }

    public void setEquipment(ArrayList<String> equipment) {
        this.equipment = equipment;
    }

    public String getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(String heroClass) {
        this.heroClass = heroClass;
    }
}
